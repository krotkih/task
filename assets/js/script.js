﻿$(document).ready(function ()
        {
            $("[data-translate-html]").each(function (idx, item) {
                $(item).html(getText($(item).attr('data-translate-html')));
            });
            $("[data-translate-value]").each(function (idx, item) {
                $(item).val(getText($(item).attr('data-translate-value')));
            });
            initSelect();
        });


        var traslations = [
            {
                "key": "title",
                "value":
                    {
                        "da": "Hvad jeg har lavet og hvad jeg kan",
                        "en": "This is what I do"
                    }
            },
            {
                "key": "text",
                "value":
                    {
                        "da": "Hvad jeg har lavet og hvad jeg kan1",
                        "en": "This is what I do 1"
                    }
            },
            {
                "key": "sss",
                "value":
                {
                    "da": "Disabled text",
                    "en": "Disabled text EN"
                }
            },
            
            {
                "key": "title1",
                "value":
                    {
                        "da": "Hej, jeg hedder Alina Krotkih og jeg er ",
                        "en": "Hello,my name is Alina Krotkih and I´m a "
                    }
            },
            {
                "key": "text1",
                "value":
                    {
                        "da": "Hej, jeg hedder Alina Krotkih og jeg er 1",
                        "en": "Hello,my name is Alina Krotkih and I´m a1"
                    }
            },
            {
                "key": "sss1",
                "value":
                {
                    "da": "Disabled text",
                    "en": "Disabled text EN"
                }
            },
            {
                "key": "title2",
                "value":
                    {
                        "da": "Frontend Udvikler",
                        "en": "Frontend Developer "
                    }
            },
            {
                "key": "text2",
                "value":
                    {
                        "da": "Frontend Udvikler1",
                        "en": "Frontend Developer1"
                    }
            },
            {
                "key": "sss2",
                "value":
                {
                    "da": "Disabled text",
                    "en": "Disabled text EN"
                }
            },
            {
                "key": "title3",
                "value":
                    {
                        "da": "kreativ og lydhør Web Design",
                        "en": "creating modern and responsive Web Design "
                    }
            },
            {
                "key": "text2",
                "value":
                    {
                        "da": "kreativ og lydhør Web Design1",
                        "en": "creating modern and responsive Web Design1"
                    }
            },
             {
                "key": "title4",
                "value":
                    {
                        "da": "Ingen lyser en lampe for at skjule den bag døren: Formålet med lys er at skabe mere lys, for at åbne folks øjne for at afsløre forundringerne rundt.",
                        "en": "No one lights a lamp in order to hide it behind the door: the purpose of light is to create more light, to open people’s eyes, to reveal the marvels around. "
                    }
            },
            {
                "key": "text4",
                "value":
                    {
                        "da": "Ingen lyser en lampe for at skjule den bag døren: Formålet med lys er at skabe mere lys, for at åbne folks øjne for at afsløre forundringerne rundt.1",
                        "en": "No one lights a lamp in order to hide it behind the door: the purpose of light is to create more light, to open people’s eyes, to reveal the marvels around.1"
                    }
            },
            {
                "key": "sss4",
                "value":
                {
                    "da": "Disabled text",
                    "en": "Disabled text EN"
                }
            }
                    
              
            ]
           
            
     
        function getSelect()
        {
            var lang = getLanguage();
            $('#langSelector').val(lang);
        }

        function languageSelect(select)
        {
            setLanguage(select.value);
            
        }

        function getText(key)
        {
            var language = getLanguage();
            var item = traslations.filter(function(t){ return t.key == key });
            if (item != null)
            {
                return item[0].value[language];
            }
        }

        function getLanguage()
        {
            var lang = readFromLocalStorage("language");
	    if (lang == null){
		lang = readCookie("language");
	    }
            if (lang != null)
            {
                return lang;
            }
            return "en";
        }

        function setLanguage(language) {
     	    if (window.localStorage == null){
	       createCookie("language", language, 1);
	    }
	    else {
               saveToLocalStorage("language", language);
            }
            window.location.reload();
        }

        function saveToLocalStorage(name, value) {
            if (window != null && window.localStorage != null)
            {
                window.localStorage.setItem(name, value);
            }
        }

        function readFromLocalStorage(name) {
            if (window.localStorage) {
                return window.localStorage.getItem(name);
            }
        }

        function createCookie(name, value, days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + value + expires + "; path=/";
        }

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }